/*
 * ---------------------------- Plugin Boilerplate -----------------------------
 * This script demonstrates how to properly instantiate an OroborosJS plugin.
 * Feel free to modify this as needed to develop your own plugins.
 * -----------------------------------------------------------------------------
 */

oroboros().plugin("demo", function (root) {
    var SIGNATURE = "Demo Plugin",
        VERSION = "0.0.1",
        AUTHOR = "Brian Dayhoff",
        LICENSE = "MIT License";

    PluginName = function (selector, context, root) {
        //do stuff here
    };

    PluginName.prototype.app = root.app;
    PluginName.prototype.constructor = PluginName;
    PluginName.prototype.SIGNATURE = SIGNATURE;
    PluginName.prototype.VERSION = VERSION;
    PluginName.prototype.AUTHOR = AUTHOR;
    PluginName.prototype.LICENSE = LICENSE;

    return PluginName;
});